<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;

class CommandHistory extends Command
{
    /**
     * @global var string
     */
    protected $signature = 'history:list';

    protected $description = "Show calculator history";
    protected $fileDirectory = "src/history.txt";

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $this->checkFile();
        $this->setTableHeader();
        $this->getContent();

    }

    protected function setTableHeader() {
        $this->line('+----+-----------+---------------------+--------+---------------------+---------------------+');
        $this->line('| '.'No'.' | '.'Command  '.' | '.'Description        '.' | '.'Result'.' | '.'Output             '.' | '.'Time               '.' |');
        $this->line('+----+-----------+---------------------+--------+---------------------+---------------------+');
    }

    protected function checkFile() {

        if(!file_exists($this->fileDirectory)) {
            $this->info('History is empty.');
            exit;
        } else {
            $read = file($this->fileDirectory);
            if(!count($read)) {
                $this->info('History is empty.');
                exit;
            }
        }
    }

    protected function getContent() {
        $counter = [];

        if(file_exists($this->fileDirectory)) {
            $read = file($this->fileDirectory);
            $index = 1;
            foreach($read AS $r) {

                $arrExplode = explode(';', $r);
                array_push($counter, [
                    "no" => $index,
                    "command" => $arrExplode[0],
                    "description" => $arrExplode[1],
                    "result" => $arrExplode[2],
                    "output" => $arrExplode[3],
                    "time" => $arrExplode[4],
                ]);
                $index ++;
            }
        }else {

        }
        $this->setContent($counter);

    }

    protected function setContent($contentArr) {

        foreach ($contentArr as $content) {
            echo "| ".$content['no']."  | ".$content['command']."\t | ".$content['description']."\t | ".$content['result']."\t | ".$content['output']." \t    | ".$content['time'];
        }
    }

}
