<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;

class CommandClear extends Command
{
    /**
     * @ global var string
     */
    protected $signature = 'history:clear';

    protected $description = "Clear saved history";
    protected $urlFile = "src/history.txt";


    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        file_put_contents($this->urlFile, "");
        $this->info('History cleared!');
    }

}
