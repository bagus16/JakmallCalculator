<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;

class CommandPow extends Command
{
    /**
     * @global var string
     */
    protected $signature = 'pow {base : The base number} {exp : The exponent number}';

    protected $description = "Exponent the given number";
    protected $storage = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $result = $this->main();
        echo $result . "\n";
    }

    protected function main()
    {
        $baseNumber = $this->getBase();
        $exponentNumber = $this->getExponent();

        if ($baseNumber && $exponentNumber) {
            $description = $this->generateCommand($baseNumber, $exponentNumber);
            $resultCalculation = $this->calculateAll($baseNumber, $exponentNumber);
            $result = strval($description) . " = " . strval($resultCalculation);
            $this->logFile($description, $resultCalculation, $result);
        } else {
            $this->info('Please fill your base number and exponent number..');
            exit;
        }

        return $result;
    }

    protected function getBase()
    {
        return $this->argument('base');
    }

    protected function getExponent()
    {
        return $this->argument('exp');
    }

    protected function getOperator(): string
    {
        return '^';
    }

    protected function generateCommand($base, $exp)
    {
        return $base . ' ^ ' . $exp;
    }

    /**
     * @param array $numbers
     *
     * @return float|int
     */
    protected function calculateAll($base, $exp)
    {
        $result = pow($base, $exp);
        return $result;
    }
    protected function logFile($description, $result, $output) {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');
        $this->storage = [
            'command' => $this->getCommand(),
            'description' => $description,
            'result' => $result,
            'output' => $output,
            'time' => $now
        ];
        $file    = fopen('src/history.txt', 'a');
        $content = $this->storage['command'].';'.$this->storage['description'].';'.$this->storage['result'].';'.$this->storage['output'].';'.$this->storage['time'];
        fwrite($file, $content. "\n");
        fclose($file);
    }
    protected function getCommand(): string
    {
        return 'Pow';
    }

}
